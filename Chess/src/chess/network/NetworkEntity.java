package chess.network;

import chess.board.Board;
import chess.board.Move;
import chess.gui.Table;
import chess.gui.Table.MoveLog;
import chess.player.MoveTransition;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public abstract class NetworkEntity extends Thread {

    protected ObjectOutputStream outputStream;
    protected ObjectInputStream inputStream;
    protected Socket connectionHandle;
    protected Object receivedMessage;

    protected String chatMessage;

    NetworkEntity(final String name) {
        super(name);
    }

    public abstract void run();

    public void getStreams() throws IOException {
        outputStream = new ObjectOutputStream(connectionHandle
                .getOutputStream());
        outputStream.flush();
        inputStream = new ObjectInputStream(connectionHandle
                .getInputStream());
    }

    public void closeConnection() {

        try {
            if (outputStream != null) {
                outputStream.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
            if (connectionHandle != null) {
                connectionHandle.close();
                chatMessage = "Connection closed with " + connectionHandle.getInetAddress().getHostName();
            }
        } catch (IOException e) {
            chatMessage = "Problems experienced when closing connection";
        }
    }

    public void processIncomingData() throws IOException {

        do {
            try {
                receivedMessage = inputStream.readObject();
            } catch (ClassNotFoundException e) {
                chatMessage = "Error: message from " + connectionHandle.getInetAddress() + " not received.";
            }
            if (receivedMessage instanceof Move) {
                final Move m = (Move) receivedMessage;
                Table.get().requestMove(m);
                Table.get().repaint();
            }
        } while (receivedMessage != null);

    }

    public void sendData(final Object obj_to_send) {
        try {
            outputStream.writeObject(obj_to_send);
            outputStream.flush();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
