package chess.network;

import chess.gui.Table;
import java.io.IOException;
import java.net.ServerSocket;
import javax.swing.JTextArea;

public class Server extends NetworkEntity {

    private ServerSocket server;

    private final int listenPort;
    
    private boolean connectionEstablished = false;
    
    private String hostName;

    public boolean isConnectionEstablished() {
        return connectionEstablished;
    }

    public Server(final int listen_port) {
        super("SERVER");
        listenPort = listen_port;
    }
    
    public void run() {

        try {
            server = new ServerSocket(listenPort, 1);
            hostName = server.getInetAddress().getHostName();
            System.out.println("Listening on port " + listenPort + " Host name: " + hostName);
            try {
                waitForConnection();
                Table.get().undoAllMoves();
                while (connectionEstablished){
                    getStreams();
                    processIncomingData();
                };
            }
            catch (IOException ioe) {
                ioe.printStackTrace();
            } finally {
                closeConnection();
            }
        } catch (IOException e) {
            System.out.println("Network Error: " + e);
        }

    }

    private void waitForConnection() throws IOException {
        connectionHandle = server.accept();
        connectionEstablished = true;
        System.out.println("Connection received from:"
                + connectionHandle.getInetAddress().getHostName());
    }

    public void closeConnection() {
        super.closeConnection();
        
        try {
            server.close();
            connectionEstablished = false;
        } catch (IOException e) {
        //    chatPanel.writeToDisplay(getName()
        //            + "failed to disconnect from the network");
        }
    }
    
    public String getHostname(){
        return this.hostName;
    }
   
}