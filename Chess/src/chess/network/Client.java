package chess.network;

import chess.gui.Table;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import javax.swing.JTextArea;

public class Client extends NetworkEntity {

    private String hostName;

    private int serverPort;
    
    private boolean connectionEstablished = false;

    public Client(final String host, final int port) {
        super("CLIENT");
        hostName = host;
        serverPort = port;
    }
    
    public void run() {
        try {
            connectToServer();
            Table.get().undoAllMoves();
            while (connectionEstablished) {
                getStreams();
                processIncomingData();
            }
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            closeConnection();
        }
    }

    private void connectToServer() {
        try {
            connectionHandle = new Socket(InetAddress.getByName(hostName),
                    serverPort);
            connectionEstablished = true;
            //chatPanel.writeToDisplay("Successfully connected to "
            //        + connectionHandle.getInetAddress().getHostName());
        } catch (IOException e) {
            //chatPanel.writeToDisplay("Failed to connect to: " + hostName);
        }
    }

}