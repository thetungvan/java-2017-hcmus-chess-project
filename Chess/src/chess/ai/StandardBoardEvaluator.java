package chess.ai;

import chess.player.Player;
import chess.board.Board;
import chess.pieces.Piece;

public final class StandardBoardEvaluator implements BoardEvaluator {
    private final static int CHECK_BONUS = 50;
    private final static int CHECK_MATE_BONUS = 10000;
    private final static int CASTLE_BONUS = 60;

    @Override
    public int evaluate(final Board board, int depth) {
        
        return scorePlayer(board,board.whitePlayer(), depth) - scorePlayer(board,board.blackPlayer(), depth);
    }
    private int scorePlayer(Board board, Player player, int depth){
        return pieceValue(player) + mobility(player) + check(player) + chessmate(player, depth) + castle(player);
    }
    
    private static int pieceValue(final Player player) {
        int pieceValueScore = 0;
        for (Piece piece: player.getActivePieces()){
            pieceValueScore += piece.getPieceValue();
        }
        return pieceValueScore;
    }

    private static int mobility(final Player player) {
        return player.getLegalMoves().size();
    }

    private static int check(final Player player) {
        return player.getOpponent().isInCheck() ? CHECK_BONUS : 0;
    }

    private static int chessmate(Player player, int depth) {
        return player.getOpponent().isInCheckMate() ? CHECK_MATE_BONUS * depthBonus(depth) : 0;
    }
    private static int depthBonus(int depth) {
        return depth == 0 ? 1 : 100 * depth;
    }

    private int castle(Player player) {
        return  player.isCastled() ? CASTLE_BONUS : 0;
    }
}
