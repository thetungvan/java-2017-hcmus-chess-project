package chess.ai;

import chess.board.Board;
import chess.board.Move;

public interface MoveStrategy {
    Move execute(Board board);
}
