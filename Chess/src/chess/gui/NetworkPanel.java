package chess.gui;

import chess.board.Move;
import chess.network.Client;
import chess.network.Server;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;

public class NetworkPanel extends JPanel {

    Server server;
    Client client;

    private static final Dimension NETWORK_PANEL_DIMENSION = new Dimension(600, 150);
    private static final Dimension CHAT_PANEL_DIMENSION = new Dimension(400, 130);
    private static final int DEFAULT_PORT = 3200;
    
    private boolean isClient = false;
    private boolean isServer = false;

    private JTextArea chatArea;

    public NetworkPanel() {
        super(new BorderLayout());
        setPreferredSize(NETWORK_PANEL_DIMENSION);
        validate();
        setVisible(true);
    }

    public void setText(String text) {
        chatArea.setText(text);
    }

    public void hostGame() {
        this.removeAll();
        this.chatArea = new JTextArea("");
        this.chatArea.setEditable(false);
        this.chatArea.setPreferredSize(CHAT_PANEL_DIMENSION);
        add(this.chatArea, BorderLayout.WEST);
        JPanel buttonPanel = new JPanel(new GridLayout(3, 1));
        JButton btnStart = new JButton("Start host");
        JButton btnCancel = new JButton("Cancel");
        btnCancel.setEnabled(false);

        btnStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnStart.setEnabled(false);
                btnCancel.setEnabled(true);
                try {
                    server = new Server(DEFAULT_PORT);
                    server.start();
                    Table.get().setIsPlayingOnLAN(true);
                    setText("Hosting game on port: " + Integer.toString(DEFAULT_PORT) + "\nHost name: 0.0.0.0");
                    isServer = true;
                } catch (Exception ex) {
                    setText("Error! Unable to start host!");
                    Table.get().setIsPlayingOnLAN(false);
                    isServer = false;
                }
            }
        });

        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    server.closeConnection();
                    btnStart.setEnabled(true);
                    btnCancel.setEnabled(false);
                    setText("Hosting stopped!");
                    Table.get().setIsPlayingOnLAN(false);
                    isServer = false;
                } catch (Exception ex) {
                    setText("Error! Unable to close connection.");
                }
            }
        });

        buttonPanel.add(btnStart);
        buttonPanel.add(btnCancel);
        this.add(buttonPanel, BorderLayout.CENTER);
        validate();
    }

    public void joinGame() {
        this.removeAll();
        this.chatArea = new JTextArea("");
        this.chatArea.setEditable(false);
        this.chatArea.setPreferredSize(CHAT_PANEL_DIMENSION);
        add(this.chatArea, BorderLayout.WEST);
        JLabel hostNameLabel = new JLabel("   Host name: ");
        JTextArea hostNameTxt = new JTextArea();
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        hostNameTxt.setBorder(BorderFactory.createCompoundBorder(border, BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        JPanel buttonPanel = new JPanel(new GridLayout(4, 1));
        JButton btnJoin = new JButton("Join host");
        JButton btnCancel = new JButton("Cancel");
        btnCancel.setEnabled(false);

        btnJoin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnJoin.setEnabled(false);
                btnCancel.setEnabled(true);
                String hostName = hostNameTxt.getText().trim();
                if (hostName.length() != 0) {
                    client = new Client(hostName, DEFAULT_PORT);
                    client.start();
                    Table.get().setIsPlayingOnLAN(true);
                    isClient = true;
                } else {
                    setText("Enter host name!!!");
                    btnJoin.setEnabled(true);
                    btnCancel.setEnabled(false);
                    Table.get().setIsPlayingOnLAN(false);
                    isClient = false;
                }
                // TODO: Join game
            }
        });

        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    setText("Disconnected from server.");
                    client.closeConnection();
                    Table.get().setIsPlayingOnLAN(false);
                    btnJoin.setEnabled(true);
                    btnCancel.setEnabled(false);
                    isClient = false;
                } catch (Exception ex) {
                    setText("Error! Cannot disconnect!");
                }

            }
        });

        buttonPanel.add(hostNameLabel);
        buttonPanel.add(hostNameTxt);
        buttonPanel.add(btnJoin);
        buttonPanel.add(btnCancel);
        this.add(buttonPanel, BorderLayout.CENTER);
        validate();
    }
    
    public void sendData(Move move){
        if (isServer){
            server.sendData(move);
        } else if (isClient){
            client.sendData(move);
        }
    }
}
