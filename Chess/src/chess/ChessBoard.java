package chess;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JComponent;

public class ChessBoard  extends JComponent{
    
    
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g); //To change body of generated methods, choose Tools | Templates
            BufferedImage chessBoardImg;
        try {
            chessBoardImg = ImageIO.read(new File("src/resource/board.png"));
            g.drawImage(chessBoardImg, 0, 0, this);
        } catch (IOException ex) {
            Logger.getLogger(ChessBoard.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
        
 }
