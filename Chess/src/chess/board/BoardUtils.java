package chess.board;

import com.google.common.collect.ImmutableMap;
import java.util.HashMap;
import java.util.Map;

public enum BoardUtils {

    INSTANCE;

    private static String[] initializeAlgebricNotation() {
        return new String[]{
            "a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8",
            "a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7",
            "a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6",
            "a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5",
            "a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4",
            "a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3",
            "a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2",
            "a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"
        };
    }

    private static Map<String, Integer> initializePositionToCoordinateMap() {
        final Map<String, Integer> positionToCoordinate = new HashMap<>();
        for (int i = 0; i < NUM_TILES; i++) {
            positionToCoordinate.put(ALGEBREIC_NOTATION[i], i);
        }
        return ImmutableMap.copyOf(positionToCoordinate);
    }

    public final boolean[] FIRST_COLUMN = initColumn(0);
    public final boolean[] SECOND_COLUMN = initColumn(1);
    public final boolean[] THIRD_COLUMN = initColumn(2);
    public final boolean[] FOURTH_COLUMN = initColumn(3);
    public final boolean[] FIFTH_COLUMN = initColumn(4);
    public final boolean[] SIXTH_COLUMN = initColumn(5);
    public final boolean[] SEVENTH_COLUMN = initColumn(6);
    public final boolean[] EIGHTH_COLUMN = initColumn(7);

    public final boolean[] EIGHTH_RANK = initRow(0);
    public final boolean[] SEVENTH_RANK = initRow(1);
    public final boolean[] SIXTH_RANK = initRow(2);
    public final boolean[] FIFTH_RANK = initRow(3);
    public final boolean[] FOURTH_RANK = initRow(4);
    public final boolean[] THIRD_RANK = initRow(5);
    public final boolean[] SECOND_RANK = initRow(6);
    public final boolean[] FIRST_RANK = initRow(7);

    public static final int NUM_TILES = 64;
    public static final int NUM_TILES_PER_ROW = 8;
    
    public static final String[] ALGEBREIC_NOTATION = initializeAlgebricNotation();
    public static final Map<String,Integer> POSITION_TO_COORDINATE = initializePositionToCoordinateMap();

    private BoardUtils() {
        //throw new RuntimeException("You cannot instantiate me!");
    }

    private static boolean[] initColumn(int columnIndex) {
        final boolean[] column = new boolean[NUM_TILES];
        for (int i = 0; i < column.length; i++) {
            column[i] = false;
        }

        do {
            column[columnIndex] = true;
            columnIndex += NUM_TILES_PER_ROW;
        } while (columnIndex < NUM_TILES);
        return column;
    }

    private static boolean[] initRow(int rowNumber) {
        final boolean[] row = new boolean[NUM_TILES];

        for (int i = 0; i < row.length; i++) {
            row[i] = false;
        }

        for (int i = 0; i < NUM_TILES_PER_ROW; i++) {
            row[(rowNumber * NUM_TILES_PER_ROW) + i] = true;
        }

        return row;
    }

    public static boolean isValidCoordinate(final int coordinate) {
        return coordinate >= 0 && coordinate < NUM_TILES;
    }
    
    public static int getCoordinateAtPosition(final String position){
        return POSITION_TO_COORDINATE.get(position);
    }
    
    public static String getPositionAtCoordinate(int destinationCoordinate) {
        return ALGEBREIC_NOTATION[destinationCoordinate];
    }

}
